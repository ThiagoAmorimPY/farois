USE [FAROIS]
GO

/****** Object:  Table [dbo].[FAROL_AMBEV]    Script Date: 05/09/2017 10:40:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[FAROL_AMBEV](
	[CdUnidadeNegocio] [varchar](7) NULL,
	[NmUnidade] [varchar](50) NULL,
	[Geo] [varchar](50) NULL,
	[DataGeracao] [datetime] NULL,
	[QtTotalRota] [int] NULL,
	[QtIniciadas] [int] NULL,
	[QtFinalizadas] [int] NULL,
	[QtNaoIniciadas] [int] NULL,
	[VlPercentual] [float] NULL,
	[VlAderenciaMes] [float] NULL,
	[VlAderenciaDMenos1] [float] NULL,
	[VlAderenciaAno] [float] NULL,
	[QtEntrega] [int] NULL,
	[QtAderencia] [int] NULL,
	[QtEntregaMes] [int] NULL,
	[QtAderenciaMes] [int] NULL,
	[QtEntregaAno] [int] NULL,
	[QtAderenciaAno] [int] NULL
) ON [PRIMARY]
GO

