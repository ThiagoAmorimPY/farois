USE [FAROIS]
GO

/****** Object:  Table [dbo].[FAROL_ROTEIRIZADOR]    Script Date: 05/09/2017 10:42:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[FAROL_ROTEIRIZADOR](
	[DiferencaKm] [int] NULL,
	[DiferencaJornada] [varchar](8) NULL,
	[UnidadeNegocio] [varchar](max) NULL,
	[PlacaVeiculo] [varchar](7) NULL,
	[PartidaRealizada] [varchar](8) NULL,
	[DistanciaRealizada] [int] NULL,
	[DistanciaPrevista] [int] NULL,
	[TempoPrevisto] [varchar](8) NULL,
	[ChegaRealizada] [varchar](8) NULL,
	[TempoRealizado] [varchar](8) NULL,
	[DataRota] [date] NULL,
	[DataGeracao] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

