USE FAROIS

SELECT	DsGeo, 
	QtTotalRota, 
	QtIniciadas, 
	QtFinalizadas, 
	QtNaoIniciadas, 
	VlPercentual,
	VlAderenciaDMenos1, 
	QtEntrega, 
	QtAderencia,
	VlAderenciaMes, 
	QtEntregaMes, 
	QtAderenciaMes,
	VlAderenciaAno, 
	QtEntregaAno, 
	QtAderenciaAno				
FROM 	FAROL_GEO R
where	cast(datageracao as date) = cast(getdate() as date)
ORDER BY VlAderenciaDMenos1 desc