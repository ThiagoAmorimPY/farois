USE FAROIS 

DECLARE @unidade VARCHAR(MAX)
DECLARE @nomeUnidade VARCHAR(MAX)
DECLARE @Geo VARCHAR(MAX) 
DECLARE @dataFim DATE
DECLARE @dataIni DATE
DECLARE @dataIniAderencia DATE
DECLARE @dataFimAderencia DATE
DECLARE @percentual FLOAT
DECLARE @total INT
DECLARE @iniciadas INT
DECLARE @finalizadas INT
DECLARE @naoIniciadas INT
DECLARE @aderenciaMes FLOAT
DECLARE @aderenciaDMenos1 FLOAT
DECLARE @aderenciaAno FLOAT
DECLARE @temRotaHoje INT
DECLARE @teveRotaOntem INT
DECLARE @diaSemana INT
DECLARE @QtEntrega INT
DECLARE @QtAderencia INT
DECLARE @QtEntregaMes INT
DECLARE @QtAderenciaMes INT
DECLARE @QtEntregaAno INT
DECLARE @QtAderenciaAno INT
DECLARE @DataGeracao DATETIME


SET @dataIni = DATEADD(DAY, - DATEPART(dd,GETDATE() -1), GETDATE())
SET @dataFim = CAST(GETDATE() AS DATE)
SET @dataFimAderencia =
					(
					CASE WHEN DATEPART(WEEKDAY, DATEADD(DAY, -1, @dataFim)) = 1
					THEN
						DATEADD(DAY, -3, @dataFim)
					ELSE
						DATEADD(DAY, -1, @dataFim)
					END
					)
SET @datageracao = getdate()

DECLARE unidades CURSOR FOR

select 	c.CdUnidadeNegocio, substring(c.NmUnidadeNegocio,1,25), a.DsGrupoUnidadeNegocio 
from	[FCIAMB\AMBEVMDM].[HBMDM_SAAS].OPMDM.tb_grupo_unidade_negocio a with (nolock),
		[FCIAMB\AMBEVMDM].[HBMDM_SAAS].OPMDM.TB_GRUPO_UNIDADE_NEGOCIO_UNIDADE b with (nolock),
		[FCIAMB\AMBEVMDM].[HBMDM_SAAS].OPMDM.TB_UNIDADE_NEGOCIO c with (nolock),
		[FCIAMB\AMBEVMDM].[HBMDM_SAAS].OPMDM.TB_STATUS_UNIDADE_NEGOCIO d with (nolock)
where	a.CdGrupoUnidadeNegocio = b.CdGrupoUnidadeNegocio
and		b.CdUnidadeNegocio = c.CdUnidadeNegocio
and		c.CdUnidadeNegocio = d.CdUnidadeNegocio
and		a.CdGrupoUnidadeNegocio in ('9','10','11','12','14','15','16')
and		d.CdImplantacaoUnidadeNegocio = '2'
and		c.IdAtivo = '1'
  
OPEN unidades;

FETCH NEXT FROM unidades INTO @unidade, @nomeUnidade, @Geo;

WHILE @@FETCH_STATUS = 0
BEGIN

	SET @dataIniAderencia = (Select DateAdd(yyyy, DateDiff(yyyy,0,GetDate()), 0))
	
	SET @iniciadas =	(
						SELECT CAST(COUNT(1) AS FLOAT)
						FROM [FCIAMB\AMBEVMDM].[HBMDM_SAAS].OPMDM.TB_ROTA R  with (nolock)
						INNER JOIN [FCIAMB\AMBEVMDM].[HBMDM_SAAS].OPMDM.TB_VEICULO V ON R.CdPlacaVeiculo = V.CdPlacaVeiculo
						WHERE V.IdRastreado = 1
						AND R.CdUnidadeNegocio = @unidade
						AND R.cdtipofrota <> 3 -- Freteiro
						AND IdTipoRota = 0
						AND R.CdPlacaVeiculo NOT IN ('REC0001')
						AND (DtPartidaRealizada IS NOT NULL AND DTCHEGADAREALIZADA IS NULL)
						AND DtRota = @dataFim
						AND IdExpurgada = 0
						)
	SET @finalizadas =	(
						SELECT CAST(COUNT(1) AS FLOAT)
						FROM [FCIAMB\AMBEVMDM].[HBMDM_SAAS].OPMDM.TB_ROTA R with (nolock)
						INNER JOIN [FCIAMB\AMBEVMDM].[HBMDM_SAAS].OPMDM.TB_VEICULO V ON R.CdPlacaVeiculo = V.CdPlacaVeiculo
						WHERE v.IdRastreado = 1
						AND r.CdUnidadeNegocio = @unidade
						AND R.cdtipofrota <> 3 -- Freteiro
						AND IdTipoRota = 0
						AND R.CdPlacaVeiculo NOT IN ('REC0001')
						AND (DtChegadaRealizada IS NOT NULL AND DtPartidaRealizada IS NOT NULL)
						AND DtRota = @dataFim
						AND IdExpurgada = 0
						)
	SET @naoIniciadas = (
						SELECT CAST(COUNT(1) AS FLOAT)
						FROM [FCIAMB\AMBEVMDM].[HBMDM_SAAS].OPMDM.TB_ROTA R with (nolock)
						INNER JOIN [FCIAMB\AMBEVMDM].[HBMDM_SAAS].OPMDM.TB_VEICULO V ON R.CdPlacaVeiculo = V.CdPlacaVeiculo
						WHERE v.IdRastreado = 1
						AND r.CdUnidadeNegocio = @unidade
						AND R.cdtipofrota <> 3 -- Freteiro
						AND IdTipoRota = 0
						AND R.CdPlacaVeiculo NOT IN ('REC0001')
						AND (DtPartidaRealizada IS NULL)
						AND DtRota = @dataFim
						AND IdExpurgada = 0
						)
	SET @total =		(
						SELECT CAST(COUNT(1) AS FLOAT)
						FROM [FCIAMB\AMBEVMDM].[HBMDM_SAAS].OPMDM.TB_ROTA R with (nolock)
						INNER JOIN [FCIAMB\AMBEVMDM].[HBMDM_SAAS].OPMDM.TB_VEICULO V ON R.CdPlacaVeiculo = V.CdPlacaVeiculo
						WHERE V.IdRastreado = 1
						AND R.CdUnidadeNegocio = @unidade
						AND R.cdtipofrota <> 3 -- Freteiro
						AND IdTipoRota = 0
						AND R.CdPlacaVeiculo NOT IN ('REC0001')
						AND DtRota = @dataFim
						AND IdExpurgada = 0
						)
	SET @percentual =	(						
						select ISNULL((SELECT CAST(@naoIniciadas AS FLOAT))/nullif((SELECT CAST(@total AS FLOAT)),0),0)
						)
	SET @aderenciaMes =	(
							SELECT ISNULL(ROUND(SUM(CAST(IdPosicaoCorreta AS DECIMAL))/COUNT(1),2),0)
							FROM [FCIAMB\AMBEVMDM].[HBMDM_SAAS].OPMDM.TB_ROTA R with (nolock)
							JOIN [FCIAMB\AMBEVMDM].[HBMDM_SAAS].OPMDM.TB_ENTREGA E with (nolock) ON R.CdRota = E.CdRota
							JOIN [FCIAMB\AMBEVMDM].[HBMDM_SAAS].OPMDM.TB_VEICULO V with (nolock) ON R.CdPlacaVeiculo = V.CdPlacaVeiculo
							WHERE r.CdUnidadeNegocio = @unidade
							AND R.DtRota BETWEEN @dataIni AND @dataFimAderencia
							AND IdRastreado = 1
							AND R.cdtipofrota <> 3 -- Freteiro
							AND R.CdPlacaVeiculo NOT IN ('REC0001')
							AND IdExpurgada = 0
						)
	SET @aderenciaDMenos1 = (
							SELECT ISNULL(ROUND(SUM(CAST(IdPosicaoCorreta AS DECIMAL))/COUNT(1),2),0)
							FROM [FCIAMB\AMBEVMDM].[HBMDM_SAAS].OPMDM.TB_ROTA R with (nolock)
							JOIN [FCIAMB\AMBEVMDM].[HBMDM_SAAS].OPMDM.TB_ENTREGA E with (nolock) ON R.CdRota = E.CdRota
							JOIN [FCIAMB\AMBEVMDM].[HBMDM_SAAS].OPMDM.TB_VEICULO V with (nolock) ON R.CdPlacaVeiculo = V.CdPlacaVeiculo
							WHERE r.CdUnidadeNegocio = @unidade
							AND R.DtRota = @dataFimAderencia
							AND IdRastreado = 1
							AND R.cdtipofrota <> 3 -- Freteiro
							AND R.CdPlacaVeiculo NOT IN ('REC0001')
							AND IdExpurgada = 0
						)
	SET @aderenciaAno = (
							SELECT ISNULL(ROUND(SUM(CAST(IdPosicaoCorreta AS DECIMAL))/COUNT(1),2),0)
							FROM [FCIAMB\AMBEVMDM].[HBMDM_SAAS].OPMDM.TB_ROTA R with (nolock)
							JOIN [FCIAMB\AMBEVMDM].[HBMDM_SAAS].OPMDM.TB_ENTREGA E with (nolock) ON R.CdRota = E.CdRota
							JOIN [FCIAMB\AMBEVMDM].[HBMDM_SAAS].OPMDM.TB_VEICULO V with (nolock) ON  R.CdPlacaVeiculo = V.CdPlacaVeiculo
							WHERE r.CdUnidadeNegocio = @unidade
							AND DtRota BETWEEN @dataIniAderencia AND @dataFimAderencia
							AND IdRastreado = 1
							AND R.cdtipofrota <> 3 -- Freteiro
							AND R.CdPlacaVeiculo NOT IN ('REC0001')
							AND IdExpurgada = 0
						)
	SET @QtEntrega = (
						SELECT COUNT(isnull(1,0))
						FROM [FCIAMB\AMBEVMDM].[HBMDM_SAAS].OPMDM.TB_ENTREGA E with (nolock)
						INNER JOIN [FCIAMB\AMBEVMDM].[HBMDM_SAAS].OPMDM.TB_ROTA R with (nolock) ON E.CDROTA = R.CDROTA
						INNER JOIN [FCIAMB\AMBEVMDM].[HBMDM_SAAS].OPMDM.TB_VEICULO V with (nolock) ON R.CdPlacaVeiculo = V.CdPlacaVeiculo
						INNER JOIN [FCIAMB\AMBEVMDM].[HBMDM_SAAS].OPMDM.TB_UNIDADE_NEGOCIO U ON R.CDUNIDADENEGOCIO = U.CDUNIDADENEGOCIO
						WHERE v.IdRastreado = 1
						AND r.CdUnidadeNegocio = @unidade
						AND IdTipoRota = 0
						AND R.cdtipofrota <> 3 -- Freteiro
						AND DtRota = @dataFimAderencia
						AND R.CdPlacaVeiculo NOT IN ('REC0001')
						AND IdExpurgada = 0
						And u.IDativo = 1						
						)
	SET @QtAderencia = (
						SELECT COUNT(isnull(1,0))
						FROM [FCIAMB\AMBEVMDM].[HBMDM_SAAS].OPMDM.TB_ENTREGA E with (nolock)
						INNER JOIN [FCIAMB\AMBEVMDM].[HBMDM_SAAS].OPMDM.TB_ROTA R with (nolock) ON E.CDROTA = R.CDROTA
						INNER JOIN [FCIAMB\AMBEVMDM].[HBMDM_SAAS].OPMDM.TB_VEICULO V with (nolock) ON R.CdPlacaVeiculo = V.CdPlacaVeiculo
						INNER JOIN [FCIAMB\AMBEVMDM].[HBMDM_SAAS].OPMDM.TB_UNIDADE_NEGOCIO U ON R.CDUNIDADENEGOCIO = U.CDUNIDADENEGOCIO
						WHERE v.IdRastreado = 1
						AND r.CdUnidadeNegocio = @unidade
						AND IdTipoRota = 0
						AND R.cdtipofrota <> 3 -- Freteiro
						AND DtRota = @dataFimAderencia
						AND R.CdPlacaVeiculo NOT IN ('REC0001')
						AND IdExpurgada = 0
						and IdPosicaoCorreta = 1
						And u.IDativo = 1
						)	
	SET @QtEntregaMes =	(
							SELECT  COUNT(1)
							FROM [FCIAMB\AMBEVMDM].[HBMDM_SAAS].OPMDM.TB_ROTA R with (nolock)
							JOIN [FCIAMB\AMBEVMDM].[HBMDM_SAAS].OPMDM.TB_ENTREGA E with (nolock) ON R.CdRota = E.CdRota
							JOIN [FCIAMB\AMBEVMDM].[HBMDM_SAAS].OPMDM.TB_VEICULO V with (nolock) ON R.CdPlacaVeiculo = V.CdPlacaVeiculo
							WHERE r.CdUnidadeNegocio = @unidade
							AND R.DtRota BETWEEN @dataIni AND @dataFimAderencia
							AND IdRastreado = 1
							AND R.cdtipofrota <> 3 -- Freteiro
							AND R.CdPlacaVeiculo NOT IN ('REC0001')
							AND IdExpurgada = 0					
						)
	SET @QtAderenciaMes =	(
							SELECT  COUNT(1)
							FROM [FCIAMB\AMBEVMDM].[HBMDM_SAAS].OPMDM.TB_ROTA R with (nolock)
							JOIN [FCIAMB\AMBEVMDM].[HBMDM_SAAS].OPMDM.TB_ENTREGA E with (nolock) ON R.CdRota = E.CdRota
							JOIN [FCIAMB\AMBEVMDM].[HBMDM_SAAS].OPMDM.TB_VEICULO V with (nolock) ON R.CdPlacaVeiculo = V.CdPlacaVeiculo
							WHERE r.CdUnidadeNegocio = @unidade
							AND R.DtRota BETWEEN @dataIni AND @dataFimAderencia
							AND IdRastreado = 1
							AND R.cdtipofrota <> 3 -- Freteiro
							AND R.CdPlacaVeiculo NOT IN ('REC0001')
							AND IdExpurgada = 0
							AND IdPosicaoCorreta = 1
						)						
	SET @QtEntregaAno = (
							SELECT COUNT(1)
							FROM [FCIAMB\AMBEVMDM].[HBMDM_SAAS].OPMDM.TB_ROTA R with (nolock)
							JOIN [FCIAMB\AMBEVMDM].[HBMDM_SAAS].OPMDM.TB_ENTREGA E with (nolock) ON R.CdRota = E.CdRota
							JOIN [FCIAMB\AMBEVMDM].[HBMDM_SAAS].OPMDM.TB_VEICULO V with (nolock) ON R.CdPlacaVeiculo = V.CdPlacaVeiculo
							WHERE r.CdUnidadeNegocio = @unidade
							AND DtRota BETWEEN @dataIniAderencia AND @dataFimAderencia
							AND IdRastreado = 1
							AND R.cdtipofrota <> 3 -- Freteiro
							AND R.CdPlacaVeiculo NOT IN ('REC0001')
							AND IdExpurgada = 0
						)						
	SET @QtAderenciaAno = (				
							SELECT COUNT(1)
							FROM [FCIAMB\AMBEVMDM].[HBMDM_SAAS].OPMDM.TB_ROTA R with (nolock)
							JOIN [FCIAMB\AMBEVMDM].[HBMDM_SAAS].OPMDM.TB_ENTREGA E with (nolock) ON R.CdRota = E.CdRota
							JOIN [FCIAMB\AMBEVMDM].[HBMDM_SAAS].OPMDM.TB_VEICULO V with (nolock) ON R.CdPlacaVeiculo = V.CdPlacaVeiculo
							WHERE r.CdUnidadeNegocio = @unidade
							AND DtRota BETWEEN @dataIniAderencia AND @dataFimAderencia
							AND IdRastreado = 1
							AND R.cdtipofrota <> 3 -- Freteiro
							AND R.CdPlacaVeiculo NOT IN ('REC0001')
							AND IdExpurgada = 0
							AND IdPosicaoCorreta = 1
						)
						
	INSERT INTO FAROL_AMBEV
	VALUES (@unidade, @nomeUnidade,@Geo, @DataGeracao, @total, @iniciadas,  @finalizadas, @naoIniciadas,
			@percentual, @aderenciaMes, @aderenciaDMenos1, @aderenciaAno, @QtEntrega, @QtAderencia, @QtEntregaMes, @QtAderenciaMes,
		    @QtEntregaAno, @QtAderenciaAno)
	FETCH NEXT FROM unidades INTO @unidade, @nomeUnidade, @Geo

END

CLOSE unidades;
DEALLOCATE unidades;
GO