USE FAROIS

SELECT	Geo, 
		NmUnidade, 
		VlAderenciaDMenos1,
		QtEntrega, 
		QtAderencia,
		VlAderenciaMes,
		QtEntregaMes,
		QtAderenciaMes,
	    VlAderenciaAno,
		QtEntregaAno,
		QtAderenciaAno,
		QtTotalRota, 
		QtIniciadas, 
		QtFinalizadas, 
		QtNaoIniciadas, 
		VlPercentual	
FROM	dbo.farol_ambev
where	cast(datageracao as date) = cast(getdate() as date)
ORDER BY VlAderenciaDMenos1 desc