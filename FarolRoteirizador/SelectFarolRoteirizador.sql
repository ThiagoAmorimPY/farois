USE FAROIS

DECLARE @date date
Set		@date = 'DATA'

select	DiferencaKm,
		DiferencaJornada,
		UnidadeNegocio,
		PlacaVeiculo,
		PartidaRealizada,
		DistanciaRealizada,
		DistanciaPrevista,
		TempoPrevisto,
		ChegaRealizada,
		TempoRealizado,
		DataRota 
from	FAROL_ROTEIRIZADOR
where	cast(datarota as date) = @date
order by 3, 4