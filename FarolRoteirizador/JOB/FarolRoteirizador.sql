USE FAROIS

DECLARE @DataGeracao datetime
SET 	@DataGeracao = getdate()

INSERT INTO FAROL_ROTEIRIZADOR
select	abs((metrosdistanciatotalroteiro / 1000) - 
		(a.VlDistanciaTotalRealizada / 1000))		'Diferen�a KM',
		'Diferen�a de Jornada' = 	CASE
										WHEN 	Convert(Char(8),(a.DtChegadaRealizada - DtPartidaRealizada),114) 
												> CONVERT(CHAR(8), DATEADD(second,segundostempoemrota,0),108)
											THEN 	Convert(Char(8),
													((a.DtChegadaRealizada - DtPartidaRealizada) 
													- (DATEADD(second,segundostempoemrota,0))),114)
										ELSE 		Convert(Char(8),
													((DATEADD(second,segundostempoemrota,0))
													- (a.DtChegadaRealizada - DtPartidaRealizada)),114)	
									END,	
		b.NmUnidadeNegocio							'Unidade Negocio',
		a.CdPlacaVeiculo							'Placa Veiculo',
		Convert(Char(8),a.DtPartidaRealizada,114)	'Partida Realizada', 
		(a.VlDistanciaTotalRealizada / 1000)		'Distancia Realizada',
		(metrosdistanciatotalroteiro / 1000)		'Distancia Prevista',
		CONVERT(CHAR(8),
		DATEADD(second,segundostempoemrota,0),108)	'Tempo Previsto',
		Convert(Char(8),a.DtChegadaRealizada,114)	'Chegada Realizada',	
		Convert(Char(8),(a.DtChegadaRealizada - 
		DtPartidaRealizada),114)					'Tempo Realizada',	
		cast(a.DtRota as date)						'Data Rota',
		@DataGeracao								'Data Gera��o'

from	[FCIAMB\AMBEVMDM].[HBMDM_SAAS].OPMDM.tb_rota a				with (nolock),
		[FCIAMB\AMBEVMDM].[HBMDM_SAAS].OPMDM.TB_UNIDADE_NEGOCIO b	with (nolock),
		[FCIROT\ROT].[mov3r-routing].dbo.relatorioroteirizacao c	with (nolock),
		[FCIROT\ROT].[mov3r-routing].dbo.deposito d					with (nolock)

where	a.CdUnidadeNegocio COLLATE SQL_Latin1_General_CP1_CI_AI		= b.CdUnidadeNegocio
and		c.iddeposito												= d.id
and		a.cdplacaveiculo COLLATE SQL_Latin1_General_CP1_CI_AI		= c.placa
and		cast(a.dtrota as date) 										= cast(c.dataroteiro as date)
and		b.IdHabilitarIntegracaoRoteirizadorHBSis					= '1'
and		a.CdSituacao 												= '3'
and		a.IdExpurgada 												= '0'
and		a.QtVolumePrevisto  										>= '75'
and		a.DtPartidaRealizada 										is not null
and		cast(a.DtRota as date)										= cast(getdate() -1 as date)
and		abs((metrosdistanciatotalroteiro / 1000) - 
		(a.VlDistanciaTotalRealizada / 1000))						<= '100'

